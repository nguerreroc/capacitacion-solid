import * as dotenv from 'dotenv';
import { ConnectionPool, Request } from 'mssql';

dotenv.config();

const options: any = {
    user: process.env.SQL_USER,
    password: process.env.SQL_PASS,
    server: process.env.SQL_SERVER,
    port: 1433,
    database: process.env.SQL_DATABASE,
    options: {
        database: process.env.SQL_DATABASE,
        encrypt: true,
    },
};

export async function getRequest(): Promise<Request> {
    const pool = await new ConnectionPool(options).connect();
    return await pool.request();
}
