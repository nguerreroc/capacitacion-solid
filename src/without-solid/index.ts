import * as fs from 'fs';
import { promisify } from 'util';
import * as sqlConnection from './sqlConnection';

const logOnFile: boolean = false;
const writeFileAsync = promisify(fs.writeFile);
const ReadFileAsync = promisify(fs.readFileSync);

const run = async () => {
    await writeFileAsync('./log.txt', 'Se inicio el proceso');
    const query = 'SELECT * FROM tiendas WHERE comuna LIKE \'' + '%viña%' + '\';';

    try {
        const request = await sqlConnection.getRequest();

        const data = await new Promise((resolve, reject) => {
            request.query(query, (err, result) => {
                if (result && result.recordset && result.recordset.length > 0) {
                    return resolve(result.recordset);
                } else {
                    writeFileAsync('./log.txt', 'No se encontraron registros');
                }

                if (err) {
                    writeFileAsync('./log.txt', err);
                    throw new Error('Problema encontrado');
                }
            });
        });

        const resultObject = {
            data,
            status: {
                hasError: false,
                code: 0,
                message: 'OK',
            },
        };

        console.log(JSON.stringify(resultObject));
        await writeFileAsync('./log.txt', JSON.stringify(resultObject));
    } catch (error) {
        if (logOnFile) {
            await writeFileAsync('./log.txt', error);
        } else {
            console.error(error);
        }
    }
};

run();
